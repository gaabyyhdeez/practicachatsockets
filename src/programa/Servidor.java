package programa;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import java.awt.Font;


public class Servidor extends JFrame
{
    public static final int HOST_MODE=0;
    public static final int CLIENT_MODE=1;
    JButton btn_send;
    JScrollPane jScrollPane1;
    JTextArea jTextArea1;
    JLabel nomServidor;
    JTextField txt_mymsg;
    int mode;
    String Name;
    String roomname;
    InetAddress hostip;
    Servidor pt;
    DatagramSocket socket;
    ArrayList<client> ClientList;
    byte[] b;
   
	public Servidor(String myname,int mod,String ip,String room)
	{
	     try
	     {
	        Name=myname;
	        mode=mod;
	        hostip=InetAddress.getByName(ip);
	        roomname=room;
	        getContentPane().setLayout(null);
	        setSize(400,460);
	        nomServidor = new JLabel("",SwingConstants.CENTER);
	        nomServidor.setFont(new Font("Leelawadee UI Semilight", Font.BOLD, 20));
	        txt_mymsg = new JTextField();
	        btn_send = new JButton("Enviar");
	        jScrollPane1 = new JScrollPane();
	        jTextArea1 = new JTextArea(8,15);
	        ClientList=new ArrayList<>();
	        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
	        getContentPane().add(nomServidor);
	        nomServidor.setBounds(136,23,220,40);
	        getContentPane().add(txt_mymsg);
	        pt=this;
	        txt_mymsg.setBounds(12,370,getWidth()-130,30);
	        getContentPane().add(btn_send);
	        btn_send.setBounds(txt_mymsg.getWidth()+20,txt_mymsg.getY(),80,30);
	        jScrollPane1.setViewportView(jTextArea1);
	        getContentPane().add(jScrollPane1);
	        jScrollPane1.setBounds(10,90,360,267);
	        btn_send.setEnabled(false);
	        jTextArea1.setEditable(false);
	        txt_mymsg.setEnabled(false);
	        
	        JLabel label = new JLabel("");
	        //label.setIcon(new ImageIcon(Servidor.class.getResource("/icono/server1.png")));
	        label.setBounds(37, 13, 87, 64);
	        getContentPane().add(label);
	        
	        btn_send.addActionListener(new ActionListener() 
	        {
	        	public void actionPerformed(ActionEvent e) {
	        		String s=txt_mymsg.getText();
	        		if(s.equals("")==false)
	        		{
	        			if(mode==HOST_MODE)
	        				envio(Name+": "+s);
	        			else
	        				sendToHost(Name+": "+s);
	        			txt_mymsg.setText("");
	        		}
	            }
	        });
	       
	        if(mode==HOST_MODE)
	        {
	            socket=new DatagramSocket(37988);
	            nomServidor.setText("Servidor"+"\n");
	            System.out.println("IP del servidor: "+InetAddress.getLocalHost().getHostAddress());
	        }
	        else
	        {
	            socket=new DatagramSocket();
	            String reqresp="!!^^"+Name+"^^!!";
	            DatagramPacket pk=new DatagramPacket(reqresp.getBytes(),reqresp.length(),hostip,37988);
	            socket.send(pk);
	            b=new byte[300];
	            pk=new DatagramPacket(b,300);
	            socket.setSoTimeout(6000);
	            socket.receive(pk);
	            reqresp=new String(pk.getData());
	            
	            if(reqresp.contains("!!^^"))
	            {
	                roomname=reqresp.substring(4,reqresp.indexOf("^^!!"));
	                nomServidor.setText(""+roomname);
	                btn_send.setEnabled(true);
	                txt_mymsg.setEnabled(true);
	            }
	            else
	            {
	                JOptionPane.showMessageDialog(pt,"El servidor no responde");System.exit(0);
	            }
	        }
	        Mensajes.start();
	     }catch(Exception ex){JOptionPane.showMessageDialog(null,ex);}
	}
	
	
	public static void main(String args[]) 
	{
	        try 
	        {
	        	String host="",room="";
	        	String name="Servidor";
	        	//ImageIcon icon = new ImageIcon("src/icono/server1.png");
	        	int mode= JOptionPane.showConfirmDialog(null, "�Desea iniciar el servidor?", "", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);     	
	        	if(mode==1)
	            {
	        		System.out.println("Ha decidido no iniciar el servidor");
	            }
	        	else
	        	{
	        		System.out.println("Ha iniciado el servidor");
	        		room="C�mputo Distribuido";
		        	Servidor obj= new Servidor(name,mode,host,room);
	        		obj.setVisible(true);
	        		
	        	}
	        } catch (Exception ex) {JOptionPane.showMessageDialog(null,ex);}
	}
	
	public void envio(String str)
	{
		try 
		{
			DatagramPacket pack=new DatagramPacket(str.getBytes(),str.length());
			
			for(int i=0;i<ClientList.size();i++)
			{
				pack.setAddress(InetAddress.getByName(ClientList.get(i).ip));
				pack.setPort(ClientList.get(i).port);
				socket.send(pack);
			}
			jTextArea1.setText(jTextArea1.getText()+"\n"+str);
		} catch (Exception ex) {JOptionPane.showMessageDialog(pt,ex);}
	}
	
	public void sendToHost(String str)
	{
		DatagramPacket pack=new DatagramPacket(str.getBytes(),str.length(),hostip,37988);
		try {socket.send(pack);} catch (Exception ex)
		{JOptionPane.showMessageDialog(pt,"El mensaje no puede enviarse");}
	}
	
	Thread Mensajes=new Thread()
	{
		public void run()
		{
			try 
			{
				while(true)
				{
					b=new byte[300];
					DatagramPacket pkt=new DatagramPacket(b,300);
					socket.setSoTimeout(0);
					socket.receive(pkt);
					String s=new String(pkt.getData());
	    
					if(mode==HOST_MODE)
					{
						if(s.contains("!!^^"))
						{
							client temp=new client();
							temp.ip=pkt.getAddress().getHostAddress();
							temp.port=pkt.getPort();
							envio(s.substring(4,s.indexOf("^^!!"))+" est� conectado(a).");
							ClientList.add(temp);
							s="!!^^"+roomname+"^^!!";
							pkt=new DatagramPacket(s.getBytes(),s.length(),InetAddress.getByName(temp.ip),temp.port);
							socket.send(pkt);
							btn_send.setEnabled(true);
							txt_mymsg.setEnabled(true);
						}
						else
						{
							envio(s);
						}
					}
					else
					{
						jTextArea1.setText(jTextArea1.getText()+"\n"+s);
					}
				}
			}catch (IOException ex) {JOptionPane.showMessageDialog(pt,ex);System.exit(0);}
		}
	};
}

